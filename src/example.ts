import { create, IndexedCollection, IndexedEntry, insert, Key, keys, match, values } from "."

type Employee = {
  firstName: string
  lastName: string
}

const collection: IndexedCollection<Employee> = create<Employee>()

const key1: Key<Employee> = insert(collection, {
  firstName: "Ron",
  lastName: "Don Jon",
})

const key2: Key<Employee> = insert(collection, {
  firstName: "Bob",
  lastName: "Muller",
})

const key3: Key<Employee> = insert(collection, {
  firstName: "Stacy",
  lastName: "Klein",
})

const mullers: IndexedEntry<Employee>[] = match(collection, ({ lastName }) => lastName === "Muller")
const allEmployees: Employee[] = values(collection)
const allKeys: Key<Employee>[] = keys(collection)
