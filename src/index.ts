type Writable<T> = { -readonly [P in keyof T]: Writable<T[P]> }

export type Key<T> = string & {
  readonly brand: T
}

export type IndexedEntry<T> = {
  readonly key: Key<T>
  readonly value: T
}

export type IndexedCollection<T> = {
  readonly sequence: number
  readonly entries: Readonly<Record<Key<T>, IndexedEntry<T>>>
}

export type Matcher<T> = (value: T) => boolean

export function create<T>(): IndexedCollection<T> {
  return {
    sequence: 0,
    entries: {},
  }
}

export function insert<T, U extends T>(c: IndexedCollection<T>, value: U): Key<T> {
  // derived type `U` is *needed* to enforce type constraints of `T` on `value`
  const w = c as Writable<IndexedCollection<T>>
  const key = (++w.sequence).toString() as Key<T>
  w.entries[key] = {
    key,
    value,
  }
  return key
}

export function has<T>(c: IndexedCollection<T>, key: Key<T>): boolean {
  return c.entries[key] !== undefined
}

export function match<T>(c: IndexedCollection<T>, matcher: Matcher<T>): IndexedEntry<T>[] {
  return Object.values(c.entries).filter((entry) => matcher(entry.value))
}

export function remove<T>(c: IndexedCollection<T>, key: Key<T>): T {
  if (!has(c, key)) {
    throw new Error(`Invalid key "${key}"`)
  }
  const value = c.entries[key]?.value
  const w = c as Writable<IndexedCollection<T>>
  delete w.entries[key]
  return value
}

export function entry<T>(c: IndexedCollection<T>, key: Key<T>): IndexedEntry<T> {
  if (!has(c, key)) {
    throw new Error(`Invalid key "${key}"`)
  }
  return c.entries[key]
}

export function get<T>(c: IndexedCollection<T>, key: Key<T>): T {
  return entry(c, key).value
}

export function update<T, U extends T>(c: IndexedCollection<T>, key: Key<T>, value: U) {
  // derived type `U` is *needed* to enforce type constraints of `T` on `value`
  if (!has(c, key)) {
    throw new Error(`Invalid key "${key}"`)
  }
  const w = c as Writable<IndexedCollection<T>>
  w.entries[key] = {
    key,
    value,
  }
}

export function keys<T>(c: IndexedCollection<T>): Key<T>[] {
  return Object.keys(c.entries) as Key<T>[]
}

export function entries<T>(c: IndexedCollection<T>): IndexedEntry<T>[] {
  return Object.values(c.entries)
}

export function values<T>(c: IndexedCollection<T>): T[] {
  return Object.values(c.entries).map((entry) => entry.value)
}
