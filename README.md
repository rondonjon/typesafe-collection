# rondonjon-typesafe-collection

A zero-dependency, lightweight implementation of key/value maps with typesafe, auto-incrementing keys.

Packaged for ESM and CommonJS. Type definitions included.

## Reference

```ts
type Key<T> = string & {
  readonly brand: T
}
type IndexedEntry<T> = {
  readonly key: Key<T>
  readonly value: T
}
type IndexedCollection<T> = {
  readonly sequence: number
  readonly entries: Readonly<Record<Key<T>, IndexedEntry<T>>>
}
type Matcher<T> = (value: T) => boolean
function create<T>(): IndexedCollection<T>
function insert<T>(c: IndexedCollection<T>, value: T): Key<T>
function has<T>(c: IndexedCollection<T>, key: Key<T>): boolean
function match<T>(c: IndexedCollection<T>, matcher: Matcher<T>): IndexedEntry<T>[]
function remove<T>(c: IndexedCollection<T>, key: Key<T>): T
function entry<T>(c: IndexedCollection<T>, key: Key<T>): IndexedEntry<T>
function get<T>(c: IndexedCollection<T>, key: Key<T>): T
function update<T>(c: IndexedCollection<T>, key: Key<T>, value: T): void
function keys<T>(c: IndexedCollection<T>): Key<T>[]
function entries<T>(c: IndexedCollection<T>): IndexedEntry<T>[]
function values<T>(c: IndexedCollection<T>): T[]
```

## Example

```ts
type Employee = {
  firstName: string
  lastName: string
}
const collection: IndexedCollection<Employee> = create<Employee>()
const key1: Key<Employee> = insert(collection, {
  firstName: "Ron",
  lastName: "Don Jon",
})
const key2: Key<Employee> = insert(collection, {
  firstName: "Bob",
  lastName: "Muller",
})
const key3: Key<Employee> = insert(collection, {
  firstName: "Stacy",
  lastName: "Klein",
})
const mullers: IndexedEntry<Employee>[] = match(collection, ({ lastName }) => lastName === "Muller")
const allEmployees: Employee[] = values(collection)
const allKeys: Key<Employee>[] = keys(collection)
```

## History

| Version | Changes                                               |
| ------- | ----------------------------------------------------- |
| 1.0.0   | Initial version                                       |
| 1.0.1   | Add missing `package.json` fields                     |
| 2.0.0   | Full rewrite                                          |
| 2.1.0   | Add example to `README.md`                            |
| 2.1.1   | Enforce type constraints in `insert()` and `update()` |
